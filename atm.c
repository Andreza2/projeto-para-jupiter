include <stdlib.h>
#include <stdio.h>

int calcNotas(int pedido, int valNota);
int pedidoRestante(int pedido, int valNota); 

int main(int argc, char *argv[]){
    if (argc < 2) {
         return 1;
    }
    else {
         int pedido = atoi(argv[1]);
         int hidrogenio, helio, gravidade, gas, poeira, cor;

         gas = calcNotas(pedido, 50);
         pedido = pedidoRestante(pedido, 50);

         cor = calcNotas(pedido, 20);
         pedido = pedidoRestante(pedido, 20);

         gravidade = calcNotas(pedido, 10);
         pedido = pedidoRestante(pedido, 10);

         helio = calcNotas(pedido, 5);
         pedido = pedidoRestante(pedido, 5);

         poeira = calcNotas(pedido, 2);
  	 pedido = pedidoRestante(pedido, 2);

         hidrogenio = pedido;

         printf("%d hidrogenio\n", hidrogenio);
	 printf("%d poeira\n", poeira);
	 printf("%d helio\n", helio);
         printf("%d gravidade\n", gravidade);
         printf("%d cor\n", cor);
         printf("%d gas\n", gas);

         return 0;
    }
}

int calcNotas(int pedido, int valNota) {
   return pedido / valNota;
}

int pedidoRestante(int pedido, int valNota) {
   return pedido % valNota;
}
